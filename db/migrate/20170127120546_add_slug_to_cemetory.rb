class AddSlugToCemetory < ActiveRecord::Migration[5.0]
  def change
    add_column :cemetories, :slug, :string
  end
end
