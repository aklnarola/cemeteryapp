class Addlonlattocemetories < ActiveRecord::Migration[5.0]
  def change
    add_column :cemetories, :lang, :float
    add_column :cemetories, :lat,  :float
  end
end
