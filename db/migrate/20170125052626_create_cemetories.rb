class CreateCemetories < ActiveRecord::Migration[5.0]
  def change
    create_table :cemetories do |t|
      t.string :fullname
      t.date :dob
      t.date :dod
      t.text :cemetoryaddress
      t.string :relation
      t.boolean :gender
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
