require 'test_helper'

class StaticpagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get home_path
    assert_response :success
    assert_select "title", "Home | Cementry App"
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | Cementry App"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | Cementry App"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | Cementry App"
  end

end
