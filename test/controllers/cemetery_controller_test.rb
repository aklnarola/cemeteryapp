require 'test_helper'

class CemeteryControllerTest < ActionDispatch::IntegrationTest
  test "should get cemeteryform" do
    get cemetery_cemeteryform_url
    assert_response :success
  end

  test "should get cemeteryformProcess" do
    get cemetery_cemeteryformProcess_url
    assert_response :success
  end

end
