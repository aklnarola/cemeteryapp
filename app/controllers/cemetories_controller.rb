class CemetoriesController < ApplicationController
  #searching using ransack
  def index
    # for button search
  end

  #for live search with ajax
  def livesearch
    search=params[:data]
       if search.blank?
         cemetories=Cemetory.all
         gmap cemetories
        else
          cemetory=Cemetory.where('lower(fullname) LIKE ?', "%#{search}%")
          gmap cemetory
        end
        respond_to do |format|
          format.html
          format.js
        end
   end

    #method for deadbody info
    def new
      @cemetory=Cemetory.new
    end

    def create
      @cemetory=Cemetory.new(cemetory_params)
      if @cemetory.save
            flash[:success] = "Cemetory details successfully saved."
            redirect_to list_path
      else
          flash[:danger] = "Cemetory details failed."
          render 'new'
      end
    end

    #list of data
    def cemetorylist
      @cemetories=Cemetory.all
    end

#destroy the cemetery detail from list
  def destroy
    cemetory=Cemetory.find(params[:id])
    if cemetory.destroy
      flash[:success] = "Cemetory record deleted.."
      redirect_to list_path
    else
      flash[:danger] = "Cemetory record not found.."
      redirect_to list_path
    end
  end

  private
  def cemetory_params
    params.require(:cemetory).permit(:fullname, :dob, :cemetoryaddress, :gender, :user_id, :relation, :dod, :lang, :lat)
  end

  def gmap record
      @hash = Gmaps4rails.build_markers(record) do |cemetory, marker|
          marker.lat cemetory.lat
          marker.lng cemetory.lang
          marker.infowindow cemetory.fullname
      end
    end


end
