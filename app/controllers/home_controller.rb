class HomeController < ApplicationController
  def index
    @search = Cemetory.search(params[:q])
    result = @search.result
    @hash = Gmaps4rails.build_markers(result) do |r, marker|
      marker.lat r.lat
      marker.lng r.lang
      marker.infowindow r.fullname
    end
  end
end
