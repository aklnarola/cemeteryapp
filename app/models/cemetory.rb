class Cemetory < ApplicationRecord
  belongs_to :user

  validates :fullname, presence: true,length: { minimum: 3, :maximum => 20}
  validates :dob,:dod, presence: true
end
