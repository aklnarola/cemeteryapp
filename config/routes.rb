Rails.application.routes.draw do

  root :to =>'home#index'
  #cemetory controller routes
  get '/new',   to: 'cemetories#new'
  post '/create',   to: 'cemetories#create'
  get '/edit',   to: 'cemetories#edit'
  get '/list',   to: 'cemetories#cemetorylist'
  get '/destroy',   to: 'cemetories#destroy'

  get '/indexsearch', to: 'cemetories#index'

  get '/showresult', to: 'cemetories#show'
  # get '/livesearch', to: 'cemetories#livesearch'

  get "/livesearch" => 'cemetories#livesearch'
  #home routes default

  get '/home',   to: 'home#index'
  post '/home',   to: 'home#index'

  #staticpages routes
  get '/contact',   to: 'staticpages#contact'

  get '/home1',      to: 'staticpages#home'

  get '/help',      to: 'staticpages#help'

  get '/about',     to: 'staticpages#about'

  #resource for thedevise
  devise_for :users,controllers:{registrations: 'users/registrations',omniauth_callbacks: 'users/omniauth_callbacks', sessions: 'users/sessions' }

  #resource of ransack searching
  resources :cemetories do
    collection { post :search, to: 'cemetories#index' }
  end

end
